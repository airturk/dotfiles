# this script will tar each folder as a seperate tar file.
# then use gzip *.tar to zip each one.
find . -maxdepth 1 -mindepth 1 -type d -exec tar cvf {}.tar {} \;
